
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import {Device} from '../device/device.model';
import {Station} from '../device/station.model';
import { TimeInterval } from 'rxjs/internal/operators/timeInterval';
import { OsrmService } from '../map/osrm.service';
import { from } from 'rxjs';
import { DeviceService } from '../device/shared/device.service';
// import { google } from '@agm/core/services/google-maps-types';

declare var google:any;

@Component({
  selector: 'app-station-route',
  templateUrl: './station-route.component.html',
  styleUrls: ['./station-route.component.scss']
})
export class StationRouteComponent implements OnInit {
  title: string = 'My first AGM project';
  // '21.0269566666667','105.901521666667'
  lat: number = 20.961805;
  lng: number = 105.936298;
  google:any;
  isLoading:boolean = false;
  deviceList:Device[];
  deviceListShow:any[];
  // stationList:Station[] = [
  //   new Station('Đối diện Trường trung cấp cảnh sát Nhân Dân - đường 179',20.9459021,105.9322384),
  //   new Station('ĐD Trạm y tế huyện Văn Giang',20.951061,105.9449038),
  //   new Station('Gần trạm xăng dầu xã Cửu Cao',20.9573055,105.9513786),
  // ];
  stationList:Station[];
  currentStation:Station=null;
  infoWindowOpened = null;
  labelOrigin=new google.maps.Point(25,-10); 

  interval;
  durationInterval:number=30;
  currentTimeInterval:number=30;
  typeDisplay:number=1;

  stationRoute:any[];
  stationRouteDebug:any[];
  routeDepart :any[];


  constructor(private router: Router,private deviceService:DeviceService) {       
  }
  ngOnInit() {  
    this.getListStation();   
    this.staticTimer();    
  }
  getListStation(){
    this.deviceService.getAllStations()
    .subscribe((data:any)=>{ 
      this.stationList =  data.result.map(item=>{
          return {
           key:item.imei,
           lat:item.lat,          
           lng:item.lng,          
           name:item.name,          
          } as Station;         
        });
        this.currentStation = this.stationList[0]; 
        this.getData();                                               
    },(err:HttpErrorResponse)=>{
      console.log('error');
    }); 
  }
  getData(){
    this.isLoading=true;    
    this.stationRoute=[];
    this.routeDepart=[];
    this.deviceListShow=[];
    switch(this.typeDisplay){
      case 1:          
        this.deviceService.getStationRoutes(this.currentStation.key)
        .subscribe((data:any)=>{ 
            this.stationRoute =  data.result.station_route.map(item=>{
              return {
                route_name:item.route_name,            
                distance:(item.distance/1000).toFixed(2),            
                duration:item.duration,
                lat:item.lat,
                lng:item.lng,
                device_name:item.device_name,
                direction:(item.direction=='go'?"Chiều đi":"Chiều về")
              };         
            });
            
            this.deviceListShow = this.deviceListShow.concat(this.stationRoute.map(item=>{
              return {
                lat:item.lat,
                lng:item.lng,
                device_name:item.device_name,
              }; 
            }));
            if(data.result.route_depart!=undefined){
              this.routeDepart =  data.result.route_depart.map(item=>{
                return {
                  route_name:item.route_name,            
                  time_start:item.time_start,      
                  lat:item.lat,
                  lng:item.lng,
                  device_name:item.device_name, 
                  direction:"Chiều đi"                                                       
                };         
              });
              this.deviceListShow =  this.deviceListShow.concat(this.routeDepart.map(item=>{
                return {
                  lat:item.lat,
                  lng:item.lng,
                  device_name:item.device_name,
                }; 
              }));   
            }
            
           
                    
            this.isLoading=false;                                                     
        },(err:HttpErrorResponse)=>{
          console.log('error');
        }); 
        break;
      case 2:          
          this.deviceService.getStationRoutesDebug(this.currentStation.key)
          .subscribe((data:any)=>{ 
              this.stationRouteDebug =  data.result.map(item=>{
                return {
                  route_name:item.route_name,            
                  trip_name:item.trip_name,            
                  time_start:item.time_start,            
                  device_name:item.devname,                                      
                  direction:item.direction,                                      
                  display:item.display,
                  location_point:item.location_point,
                  current_point:item.current_point,
                  lat:Number.parseFloat(item.lat), 
                  lng:Number.parseFloat(item.lng),            
                };         
              });
              this.isLoading=false;       
              this.deviceListShow =  this.deviceListShow.concat(this.stationRouteDebug.map(item=>{
                return {
                  lat:item.lat,
                  lng:item.lng,
                  device_name:item.device_name,
                }; 
              }));                                     
      
          },(err:HttpErrorResponse)=>{
            console.log('error');
          }); 
        break;
    }   
  } 
  onStationChange(value){
    let indexStation = this.stationList.findIndex(station=>station.key==value);
    if(indexStation>=0){
      this.currentStation = this.stationList[indexStation];    
      this.onRefresh();  
    }

  }
  onRefresh(){
    this.getData();
    this.currentTimeInterval = this.durationInterval;
  }
  close_window(){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }  
  }
    
  select_marker(infoWindow){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }
    this.infoWindowOpened = infoWindow;    
  }
  
  staticTimer(){
    this.interval = setInterval(()=>{
        if(this.currentTimeInterval>0) this.currentTimeInterval--;
        else {
          this.getData();
          this.currentTimeInterval = this.durationInterval;
        }
    },1000);
  } 
  onTypeDisplay(value){
    this.typeDisplay= Number.parseInt(value);
    this.onRefresh();
  }
}
