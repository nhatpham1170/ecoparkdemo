import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as FileSaver from 'file-saver';
import * as XLSXJS from 'xlsx';


declare var XLSX:any;
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable()
export class ExcelService {
  constructor() { }
  public exportRoute(dataJson,fileNamePrefix) {
   
    let wb:XLSXJS.WorkBook = XLSXJS.utils.book_new();
    wb.Props = {
      Title: "SheetJS Tutorial",
      Subject: "Test",
      Author: "Red Stapler",
      CreatedDate: new Date(2017,12,19)
    };
    wb.SheetNames.push("Báo cáo tuyến");
    
    let ct_title=['Báo cáo tuyến '+ dataJson.route_name];
    let date = dataJson.date.split('-').reverse().join('/');
    let ct_date=['Ngày '+ date];
    let ct_go_success     =['Tỷ lệ chiều đi thành công: ' +dataJson.trip_go_success     +"/"+dataJson.total_trip];
    let ct_back_success   =['Tỷ lệ chiều về thành công: ' +dataJson.trip_back_success   +"/"+dataJson.total_trip];
    let ct_total_success  =['Tỷ lệ chuyến thành công: '   +dataJson.trip_success        +"/"+dataJson.total_trip];
    let ct_columname = ['STT','Tên chuyến','Tên xe','Khởi hành','Kết thúc',
    'Trạng thái đi','Điểm dừng đi','Thời gian xe tới','Trạng thái về',
    'Điểm dừng về','Thời gian xe tới'
    ];
    let ws_data = [ct_title,
      [],
      ct_date,
      [],
      ct_go_success,
      ct_back_success,
      ct_total_success,
      [],
      ct_columname];
    
    let arrMerges=[XLSXJS.utils.decode_range("A1:K1")];
    let arr_content=[];
    let lastRow=10;
    let rowForm=0;
    let rowTo=0;
    let count_stt=0;   
    let datePipe = new DatePipe("en-US"); 
    dataJson.data.forEach(function(trip){
      count_stt++;      
      let maxCount = (trip.go.calendar.length>trip.back.calendar.length?
                        trip.go.calendar.length:trip.back.calendar.length); 
      
      for(let i=0;i<maxCount;i++){
        let row_calendar = ['','','','','','','','','','',''];
        if(i==0){
          rowForm=lastRow;
          row_calendar[0]=count_stt.toString();
          row_calendar[1]=trip.trip_name.toString();
          row_calendar[2]=trip.device_name.toString();
          row_calendar[3]=trip.time_start.toString();
          row_calendar[4]=trip.time_end.toString();
          row_calendar[5]=trip.go.station_success.toString()+'/'+trip.go.total_station.toString();
          row_calendar[8]=trip.back.station_success.toString()+'/'+trip.back.total_station.toString();
        }
        // check go
        if(i<trip.go.calendar.length){
          row_calendar[6]=trip.go.calendar[i].station_name;
          row_calendar[7]= datePipe.transform(trip.go.calendar[i].date_time,'HH:mm:ss');
        }
        // check back
        if(i<trip.back.calendar.length){
          row_calendar[9]   =trip.back.calendar[i].station_name;
          row_calendar[10]  = datePipe.transform(trip.back.calendar[i].date_time,'HH:mm:ss');
        }
        ws_data.push(row_calendar);  
        rowTo = lastRow;    
        lastRow++;
          
      }
      if(rowForm>0) arr_content.push(rowForm);
      arrMerges.push( XLSXJS.utils.decode_range("A"+rowForm+":A"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("B"+rowForm+":B"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("C"+rowForm+":C"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("D"+rowForm+":D"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("E"+rowForm+":E"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("F"+rowForm+":F"+rowTo));
      arrMerges.push( XLSXJS.utils.decode_range("I"+rowForm+":I"+rowTo));      
    });
   

    let ws = XLSXJS.utils.aoa_to_sheet(ws_data); 
    ws["!cols"]=[
      {wch:5},
      {wch:15},
      {wch:25},
      {wch:10},
      {wch:10},
      {wch:12},
      {wch:25},
      {wch:14},
      {wch:12},
      {wch:25},
      {wch:14},
    ];  
    let array 
    // style
    let arrayColumnBold=['A','B','C','D','E','F','G','H','I','J','K'];
    for(let i=0;i<arrayColumnBold.length;i++){
      // style for title
      let cellName=arrayColumnBold[i]+'9';     
       ws[cellName].s={ font: { bold: true }};         
    }
    let arrayColumnCenter=['A','B','C','D','E','F','I'];
    //style for row content   
    for(let i=0;i<arrayColumnCenter.length;i++){
      for(let j=0;j<arr_content.length;j++){      
        let cellName=arrayColumnCenter[i]+arr_content[j].toString();        
          ws[cellName].s = {alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }};
      }
    }
   

    ws['!merges']=arrMerges;

    ws['A1'].s={ font: { name: 'Times New Roman', sz: 14, bold: true }, 
    alignment: { vertical: "center", horizontal: "center", wrapText: true } };    
    // ws['A**'].s={alignment: { vertical: "center", horizontal: "center", wrapText: true }};
    let defaultCellStyle = { font: { name: 'Times New Roman', sz: 12, color: { rgb: "#FF000000" }, bold: false, italic: false, underline: false }, 
    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }, 
    border: { top: { style: "thin", color: { "auto": 1 } }, right: { style: "thin", 
    color: { "auto": 1 } }, bottom: { style: "thin", color: { "auto": 1 } }, left: { style: "thin", color: { "auto": 1 } } } };   
    console.log(ws);
    wb.Sheets["Báo cáo tuyến"] = ws;
    const excelBuffer: any = this.s2ab(XLSX.write(wb, { bookType: 'xlsx', type: 'binary',cellStyles:true,defaultCellStyle:defaultCellStyle}));
    this.saveAsExcelFile(excelBuffer, fileNamePrefix+'_');  
  }
  
  private s2ab(s) {
    if (typeof ArrayBuffer !== 'undefined') {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    } else {
      let buf = new Array(s.length);
      for (let i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    // FileSaver.saveAs(new Blob([data], { type: 'application/octet-stream' }), 'exported.xlsx');
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }
}