import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent,HttpResponse,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
// import 'rxjs/add/operator/do';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    tokenKey:string='userToken';
    constructor(private router:Router){

    }
    intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>{
        
        if(req.headers.get('No-Auth')=="True"){
            return next.handle(req.clone());
        }
        // osrm
        if(req.url.includes(environment.osrm.host)){                                                      
            return next.handle(req);
        }        
        if(localStorage.getItem(this.tokenKey)!=null){                     
            const colonedreq=req.clone({
                headers: req.headers
                .set("token",localStorage.getItem('userToken'))
                .set('AppID',environment.api.AppID)
            });            
            return next.handle(colonedreq).pipe(
                map((event:HttpEvent<any>)=>{
                    if(event instanceof HttpResponse){
                        // console.log('event--->>',event);
                    }
                    return event;
                }),
                catchError((error:HttpErrorResponse)=>{
                    let data = {};
                    data = {
                        reason: error && error.error.reason ? error.error.reason : '',
                        status: error.status
                    };
                    return throwError(error);
                })
            );
        }
        else{
            this.router.navigateByUrl('/login');
        }
        return next.handle(req.clone());
    }
}