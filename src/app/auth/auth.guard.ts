import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements  CanActivate {
  constructor(private router:Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if(localStorage.getItem('userToken')!=null){
      let tokenExpire= new Date(localStorage.getItem('tokenExpire'));
      let dateNow = new Date();      
      if(dateNow>tokenExpire){
        this.router.navigate(['/login']);
      }
      return true;
    }
    this.router.navigate(['/login']);    
    return false;
  };
}
