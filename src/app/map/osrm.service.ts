import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { element } from '@angular/core/src/render3';


@Injectable({
  providedIn: 'root'
})
export class OsrmService {

  private _HOST=environment.osrm.host;
  private _ReqHeader=new HttpHeaders({'type':'osrm'});
  private point:{lat:number,lng:number};
  constructor(private http:HttpClient) { }
  getDistance(origin,destination){
    // let promiseList=[];
    // origins.forEach(element => {
    //   let promise = this.http.get('http://125.212.203.173:5000/route/v1/driving/'+element.lng+','+element.lat+';'+destination.lng+','+destination.lat+'');
    //   promiseList.push(promise);
    // });
    // return Promise.all(promiseList);
    return this.http.get('http://125.212.203.173:5000/route/v1/driving/'+origin.lng+','+origin.lat+';'+destination.lng+','+destination.lat+'');;    
    // return this.http.get('http://125.212.203.173:5000/route/v1/driving/105.8270256,20.9719461;105.8015782,20.9904764');    
  }
  routerDistances(origins:{lat:number,lng:number}[],destination:{lat:number,lng:number}){
    let promiseList=[];
    let _this=this;
    origins.forEach(element=>{
      let promise = new Promise(function(resolve,reject){
        _this.http.get(environment.osrm.host+'/route/v1/driving/'+element.lng+','+element.lat+';'+destination.lng+','+destination.lat+'')
        .subscribe(data=>{
          resolve(data);
        });
      });
      promiseList.push(promise);
    });
    return Promise.all(promiseList);
  }
}
