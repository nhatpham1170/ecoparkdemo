import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user/shared/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import {Device} from '../device/device.model';
import {Station} from '../device/station.model';
import { TimeInterval } from 'rxjs/internal/operators/timeInterval';
import { OsrmService } from '../map/osrm.service';
import { from } from 'rxjs';
// import { google } from '@agm/core/services/google-maps-types';

declare var google:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: string = 'My first AGM project';
  // '21.0269566666667','105.901521666667'
  lat: number = 20.961805;
  lng: number = 105.936298;
  google:any;
  isLoading:boolean = false;
  deviceList:Device[];
  deviceListShow:Device[];
  // stationList:Station[] = [
  //   new Station('Đối diện Trường trung cấp cảnh sát Nhân Dân - đường 179',20.9459021,105.9322384),
  //   new Station('ĐD Trạm y tế huyện Văn Giang',20.951061,105.9449038),
  //   new Station('Gần trạm xăng dầu xã Cửu Cao',20.9573055,105.9513786),
  // ];
  stationList:Station[] = [
    new Station('Gara',20.9694194,105.929927),
    new Station('Công viên mùa xuân',20.9715611,105.9320278),
    new Station('Vườn mai',20.971475,105.933825),
    new Station('Citi mark',20.9723556,105.9281389),
    new Station('Bến A2',20.9737722,105.9247083),
    new Station('Đối diện A2',20.9735944,105.9246306),
    new Station('Vườn tùng',20.9726917,105.9268833)
    
  ];
  currentStation:Station=null;
  infoWindowOpened = null;
  labelOrigin=new google.maps.Point(25,20); 

  interval;
  durationInterval:number=60;
  currentTimeInterval:number=60;
  typeDistance:number=1;

  constructor(private router: Router,private userService:UserService,private osrm:OsrmService) {       
  }
  ngOnInit() {     
    this.getData(this.stationList[0]);
    this.staticTimer();    
  }
  
  getDistances(origins,destinations){
    let service = new google.maps.DistanceMatrixService;
    var promise = new Promise(function(resolve,reject){
			service.getDistanceMatrix({
	          origins: origins,
	          destinations: destinations,
	          travelMode: 'TRANSIT',
	          // travelMode: 'DRIVING',
	          unitSystem: google.maps.UnitSystem.METRIC,
	          avoidHighways: false,
	          avoidTolls: false
	        }, function(response, status) {	          
	          if (status !== 'OK') {
	            // alert('Error was: ' + status);
	            reject(response);
	          } else {	            
	            resolve(response);	                   
	          }
	        });
      	});
      	return promise;
  }
  getDistancesOsrm(){    
    let _this = this;

    let _destination = {lat: this.currentStation.lat, lng: this.currentStation.lng};
    let _origins = this.deviceList.map(device=>{
      return {lat: device.lat, lng: device.lng}
    })    
    this.osrm.routerDistances(_origins,_destination).then(data=>{    
      // console.log(data);
      for(let i=0;i< _this.deviceList.length;i++){
            // _this.deviceList[i].address=originAddresses[i];
            let distance = data[i]['routes'][0]['distance']!=undefined?data[i]['routes'][0]['distance']:'';
            let duration = data[i]['routes'][0]['duration']!=undefined?data[i]['routes'][0]['duration']:'';
            let durationTex = '';
            if(Number.parseInt(distance)){
              durationTex =((duration/3600)>=1?((duration/3600).toFixed(0)+' h '):'') 
                  + ((duration/60)>=1?((duration/60).toFixed(0)+' m '):'') 
                  + ((duration%60)>=1?(duration%60).toFixed(0)+' s':'');
            }
            
            if(Number.parseInt(distance)){
              distance = (distance/1000).toFixed(2);
            }
            
            _this.deviceList[i].distance = distance;
            _this.deviceList[i].duration = durationTex.toString();
            _this.deviceList[i].durationValue = duration;
          } 
          _this.deviceListShow = [..._this.deviceList.sort((a, b) => (a.distance > b.distance) ? 1 : -1)];           
    }).catch(error=>{
      console.log(error);
    });    
  }
  // getDistancesRouterService(origin,destination){
  //   let service = new google.maps.DistanceMatrixService;
  //   var promise = new Promise(function(resolve,reject){
	// 		service.getDistanceMatrix({
	//           origins: origins,
	//           destinations: destinations,
	//           travelMode: 'TRANSIT',
	//           // travelMode: 'DRIVING',
	//           unitSystem: google.maps.UnitSystem.METRIC,
	//           avoidHighways: false,
	//           avoidTolls: false
	//         }, function(response, status) {	          
	//           if (status !== 'OK') {
	//             // alert('Error was: ' + status);
	//             reject(response);
	//           } else {	            
	//             resolve(response);	                   
	//           }
	//         });
  //     	});
  //     	return promise;
  // }
  onStationChange(value){
    let indexStation = this.stationList.findIndex(station=>station.key==value);
    if(indexStation>=0){
      this.getData(this.stationList[indexStation]);
    }

  }
  getData(station:Station){
    this.currentStation=station;
    this.isLoading=true;
    this.userService.getNearest(station.lat.toString(),station.lng.toString())
    .subscribe((data:any)=>{ 
        this.deviceList =  data.units.map(item=>{
          return {
            name:item.devname,            
            lat:Number.parseFloat(item.latitude), 
            lng:Number.parseFloat(item.longitude), 
            distance:Number.parseFloat(item.dis_station).toFixed(2).toString(),
            address:item.address,
            trktime:item.trktime,
          } as Device;         
        });
        this.isLoading=false;       
        this.deviceListShow = [...this.deviceList];           
        switch(this.typeDistance.toString()){
          case '1':            
              this.getDistancesOsrm();
            break;
          case '2':
            break;

        }                     
        //http://125.212.203.173:5000/route/v1/driving/105.8270256,20.9719461;105.8015782,20.9904764

        // console.log(origins);
        // console.log(destinations);
        // let promise =  this.getDistances(origins,destinations);
        // promise.then(ret=>{
        //   let rows= ret['rows'];          
        //   let originAddresses = ret['originAddresses'];
        //   console.log(ret);
        //   for(let i=0;i< originAddresses.length;i++){
        //     this.deviceList[i].address=originAddresses[i];
        //     this.deviceList[i].distance=(rows[i]['elements'][0]['distance']['text']!=undefined?rows[i]['elements'][0]['distance']['text']:'');
        //     this.deviceList[i].duration=rows[i]['elements'][0]['duration']['text'];
        //     this.deviceList[i].durationValue=rows[i]['elements'][0]['duration']['value'];
        //   }
        //   this.isLoading=false;   
        //   // sort 
        //   this.deviceListShow = this.deviceList.sort((a, b) => (a.durationValue > b.durationValue) ? 1 : -1);          
        // }).catch(error=>{  
        //   this.isLoading=false;        
        //   console.log(error);
        // }); 

    },(err:HttpErrorResponse)=>{
      console.log('error');
    });
  }
  onRefresh(){
    this.getData(this.currentStation);
    this.currentTimeInterval = this.durationInterval;
  }
  close_window(){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }  
  }
    
  select_marker(infoWindow){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }
    this.infoWindowOpened = infoWindow;    
  }
  
  staticTimer(){
    this.interval = setInterval(()=>{
        if(this.currentTimeInterval>0) this.currentTimeInterval--;
        else {
          this.getData(this.currentStation);
          this.currentTimeInterval = this.durationInterval;
        }
    },1000);
  }
  onDistanceTypeChange(value){
    this.typeDistance = value;
    this.onRefresh();
    console.log(value);
    // console.log(value);
  }  
}
