import {Routes} from '@angular/router'
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { AuthGuard } from './auth/auth.guard';
import { StationRouteComponent } from './station-route/station-route.component';
import { RouteDepartComponent } from './route-depart/route-depart.component';
import { ReportComponent } from './report/report.component';

export const appRoutes : Routes =[
    {path: 'home',component : HomeComponent,canActivate:[AuthGuard]},
    {path: 'station-route',component : StationRouteComponent,canActivate:[AuthGuard]},
    {path: 'route-depart',component : RouteDepartComponent,canActivate:[AuthGuard]},
    {path: 'report',component : ReportComponent,canActivate:[AuthGuard]},
    {
        path : 'signup',component: UserComponent,
        children: [{path:'',component : SignUpComponent}]
    },  
    {
        path : 'login',component: UserComponent,
        children: [{path:'',component : SignInComponent}]
    },    
    {
        path : '',redirectTo:'/login',pathMatch: 'full'
    }
];