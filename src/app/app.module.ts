import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { UserService } from './user/shared/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { DeviceComponent } from './device/device.component';
import { environment } from 'src/environments/environment';
import { GoogleService } from './google/google.service';
import { AgmCoreModule } from '@agm/core';
import { OsrmService } from './map/osrm.service';
import { HeaderComponent } from './header/header.component';
import { StationRouteComponent } from './station-route/station-route.component';
import { RouteDepartComponent } from './route-depart/route-depart.component';
import { DeviceService } from './device/shared/device.service';
import { ExcelService } from './shared/services/excel.service';
import { ReportComponent } from './report/report.component';
// import { MomentModule } from 'ngx-moment';
// import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';

 
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    SignInComponent,
    SignUpComponent,
    HomeComponent,
    DeviceComponent,    
    HeaderComponent, 
    StationRouteComponent, 
    RouteDepartComponent, ReportComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,   
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapKey
    }),
    
    // MomentModule
  ],
  providers : [UserService,AuthGuard,{
    provide : HTTP_INTERCEPTORS,
    useClass : AuthInterceptor,
    multi : true,
  },
  GoogleService,
  OsrmService,
  DeviceService,
  ExcelService
],
  bootstrap: [AppComponent]
})
export class AppModule {
   public static api = environment.api;
 }
