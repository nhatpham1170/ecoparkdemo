export class Device {
    name:string = "";
    address:string = "";
    distance:string ="";
    duration:string = "";    
    lat:number;    
    lng:number;    
    durationValue:number;
    id:number;
    trktime:String='';
}
