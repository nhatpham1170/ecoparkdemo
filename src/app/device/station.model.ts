
export class Station {
    key:string;
    name:string='';
    lat:number;
    lng:number;
    constructor(name:string,lat:number,lng:number){
        let date=new Date;
        this.key=Math.random()+"_"+date.getTime();
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }
}
