import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient) { }
  
  getAllRouteDepart(imei:string){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/route-depart-debug',{headers:reqHeader,params:{'imei':imei}});    
  }
  getAllRouteStarted(imei:string){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/route-started-debug',{headers:reqHeader,params:{'imei':imei}});    
  }
  getAllStations(){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/stations',{headers:reqHeader,params:{}});    
  }
  getStationRoutes(imei:string){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/station-route-web',{headers:reqHeader,params:{'imei':imei}});    
  }
  getStationRoutesDebug(imei:string){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/station-route-debug-web',{headers:reqHeader,params:{'imei':imei}});    
  }  

  getReportRoute(routeId:string,date:string){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/report-ecobus/route',{headers:reqHeader,params:{'date':date,'route_id':routeId}});        
  }  
  getAllRoute(){
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/report-ecobus/get-routes',{headers:reqHeader,params:{}});        
  }  


}
