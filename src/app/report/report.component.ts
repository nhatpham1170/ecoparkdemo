import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../device/shared/device.service';
import { ExcelService } from '../shared/services/excel.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  public dateNow: Date = new Date ();
  
  routes:any[];
  isLoading=false;

  prevtDay:Date;
  dataRoute;
  currentRoute:any=null;
  dateInput;
  countSuccess=0;  
  checkShow=false;
  
  constructor(private deviceService:DeviceService, private excelService:ExcelService) { 
    this.prevtDay = new Date();
    this.prevtDay.setDate(this.prevtDay.getDate() - 1);
    this.dateInput = this.prevtDay.toISOString().substring(0, 10);
  }
  
  ngOnInit() {
    this.deviceService.getAllRoute()
    .subscribe((data:any)=>{ 
     
        this.routes =  data.result.map(item=>{
          return {
            name:item.name,            
            id:item.id,
          };         
        });        
    });
  }
  getReportByRoute(routeId:string,date:string){
    this.isLoading=true;
    // refresh
    this.dataRoute={
      data:[],
      trip_go_success:0,
      trip_back_success:0,
      trip_success:0,
      total_trip:0,    
    };
    this.countSuccess=0;
    this.checkShow=false;
    
    this.deviceService.getReportRoute(routeId,date)
    .subscribe((data:any)=>{      
      this.dataRoute =  data.result;
        // this.dataRoute =  data.result.map(item=>{
        //   return {
        //     data:item.data,
        //     trip_go_success:item.trip_go_success,
        //     trip_back_success:item.trip_back_success,
        //     trip_success:item.trip_success,
        //     total_trip:item.total_trip,           
        //     // route_name  :item.route_name,            
        //     // trip_name   :item.trip_name,
        //     // device_name :item.device_name,
        //     // time_start  :item.time_start,
        //     // time_end    :item.time_end,
        //     // go          :item.go,
        //     // back        :item.back,
        //     // status      :item.status,
        //     // calendar    :item.calendar,
        //   };
          
        // });  
        // console.log(this.dataRoute);
        // this.countSuccess = this.dataRoute.filter(x=>x.station_success==x.total_station).length;
        this.checkShow=true;
        this.isLoading=false;       
    });
  }
  onRouteChange(value){
    
    let index = this.routes.findIndex(x=>x.id==value);
    if(index>=0){
      this.currentRoute =this.routes[index];      
      // this.onRefresh();
    }
  }
  onRefresh(){     
    if(this.currentRoute!=null){
      this.getReportByRoute(this.currentRoute.id,this.dateInput);
    }
    
  }
  exportXLSX(){
    if(this.dataRoute!=undefined)  
      this.excelService.exportRoute(this.dataRoute,'BaoCaoTuyen_'+this.dataRoute['route_name']+'_'+this.dataRoute['date'].replace(/-/g,''));
  }

}

