import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  // providers:[UserService],
})
export class SignInComponent implements OnInit {
  
  isLoginError: Boolean = false;
  constructor(private userService:UserService, private router: Router ) { }

  ngOnInit() {
  }
  onSubmit(form:NgForm){  
    this.userService.userAuthentication(form.value.userName,form.value.password)
    .subscribe((data:any)=>{
        if(data.status==200) {
          localStorage.setItem('userToken',data.token);          
          localStorage.setItem('tokenExpire',data.tokenExpire);
          localStorage.setItem('dataRoot',data);
          this.router.navigate(['/home']);
        }
        else{
          this.isLoginError=true;
        }                        
    },
    (err:HttpErrorResponse)=>{
      this.isLoginError=true;
    });    
  }
}
