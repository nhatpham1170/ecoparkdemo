import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  userAuthentication(userName,password){
    let reqHeader=new HttpHeaders()
    .set('AppID',environment.api.AppID)
    .set('Accept','application/json')
    .set('Content-Type','application/json');
    return this.http.post(environment.api.host+'/login',{'username':userName,'password':password},{headers:reqHeader});
  } 
  getDevices(userName:string,devId:string) {
    let reqHeader=new HttpHeaders({'username':'ecopark',
    'devid':'-1'});

    return this.http.get(environment.api.host+'/tracking',{headers:reqHeader});
  }
  getNearest(latitude:string,longitude:string) {
    let reqHeader=new HttpHeaders({'username':'ecopark'});

    return this.http.get(environment.api.host+'/tracking/nearest',{headers:reqHeader,params:{'latitude':latitude,'longitude':longitude}});
  }

}
