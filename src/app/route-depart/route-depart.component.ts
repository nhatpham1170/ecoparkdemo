import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user/shared/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import {Device} from '../device/device.model';
import {Station} from '../device/station.model';
import { TimeInterval } from 'rxjs/internal/operators/timeInterval';
import { OsrmService } from '../map/osrm.service';
import { from } from 'rxjs';
import { DeviceService } from '../device/shared/device.service';
// import { google } from '@agm/core/services/google-maps-types';

declare var google:any;

@Component({
  selector: 'app-route-depart',
  templateUrl: './route-depart.component.html',
  styleUrls: ['./route-depart.component.scss']
})
export class RouteDepartComponent implements OnInit {
  title: string = 'My first AGM project'; 
  lat: number = 20.961805;
  lng: number = 105.936298;
  google:any;
  isLoading:boolean = false;
  deviceList:any[];
  deviceListShow:any[];    
  currentStation:Station=null;
  infoWindowOpened = null;
  // labelOrigin=new google.maps.Point(25,20); 
  labelOrigin=new google.maps.Point(25,-10); 
  stationList:Station[];  

  interval;
  durationInterval:number=30;
  currentTimeInterval:number=30;
  typeRoute:number=1;
  

  constructor(private router: Router,private deviceService:DeviceService,private osrm:OsrmService) {       
    
  }
  ngOnInit() {    
    this.getListStation();     
    this.staticTimer();    
  }
  getListStation(){
    this.deviceService.getAllStations()
    .subscribe((data:any)=>{ 
      this.stationList =  data.result.map(item=>{
          return {
           key:item.imei,
           lat:item.lat,          
           lng:item.lng,          
           name:item.name,          
          } as Station;         
        });
        this.currentStation = this.stationList[0]; 
        this.getData();                                               
    },(err:HttpErrorResponse)=>{
      console.log('error');
    }); 
  }
  getData(){  
    this.isLoading=true;    
    switch(this.typeRoute){
      case 1:          
          this.deviceService.getAllRouteDepart(this.currentStation.key)
          .subscribe((data:any)=>{ 
              this.deviceList =  data.result.map(item=>{
                return {
                  route_name:item.route_name,            
                  trip_name:item.trip_name,            
                  time_start:item.time_start,            
                  device_name:item.device_name,            
                  trktime:item.trktime,            
                  lat:Number.parseFloat(item.lat), 
                  lng:Number.parseFloat(item.lng),            
                };         
              });
              this.isLoading=false;       
              this.deviceListShow = [...this.deviceList];                                           
      
          },(err:HttpErrorResponse)=>{
            console.log('error');
          }); 
        break;
      case 2:          
          this.deviceService.getAllRouteStarted(this.currentStation.key)
          .subscribe((data:any)=>{ 
              this.deviceList =  data.result.map(item=>{
                return {
                  route_name:item.route_name,            
                  trip_name:item.trip_name,            
                  time_start:item.time_start,            
                  device_name:item.device_name,            
                  trktime:item.trktime,            
                  lat:Number.parseFloat(item.lat), 
                  lng:Number.parseFloat(item.lng),            
                };         
              });
              this.isLoading=false;       
              this.deviceListShow = [...this.deviceList];                                           
      
          },(err:HttpErrorResponse)=>{
            console.log('error');
          }); 
        break;
    }    
       
  }
  onStationChange(value){
    let indexStation = this.stationList.findIndex(station=>station.key==value);    
    if(indexStation>=0){
      this.currentStation = this.stationList[indexStation];     
      this.onRefresh(); 
    }
  }
  onRefresh(){
    this.getData();
    this.currentTimeInterval = this.durationInterval;
  }
  close_window(){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }  
  }
    
  select_marker(infoWindow){
    if(this.infoWindowOpened!=null){
      this.infoWindowOpened.close();
    }
    this.infoWindowOpened = infoWindow;    
  }
  
  staticTimer(){
    this.interval = setInterval(()=>{
        if(this.currentTimeInterval>0) this.currentTimeInterval--;
        else {
          this.getData();
          this.currentTimeInterval = this.durationInterval;
        }
    },1000);
  }
  onRouteType(value){
    this.typeRoute= Number.parseInt(value);
    this.onRefresh();
  }
}
